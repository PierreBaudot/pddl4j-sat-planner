import os.path
import subprocess
import re
import matplotlib.pyplot as plt
from sys import argv

perfRef = re.compile(r"(plan\s*total\s*cost\s*:\s*(\d*.\d*))|((\d*.\d*)\s*seconds\s*total\s*time)")
TIMEOUT = 180
ITERATIONS = 1
MAX_FAILS = 2


def evalMeanPerf(command):
    cost = None
    timeSum = 0
    for i in range(ITERATIONS):
        print("\titeration {}".format(i))
        out = subprocess.run(command, capture_output=True, text=True).stdout
        res = perfRef.findall(out)
        if len(res) != 2:
            print("\tError or timeout, skipping")
            return None, None
        else:
            cost = float(res[0][1])
            timeSum += float(res[1][-1])

    return cost, timeSum / ITERATIONS


if __name__ == '__main__':
    benchmarks = ["blocksworld", "depot", "gripper", "logistics"]

    if len(argv) > 1:
        benchmarks = argv[1:]

    for benchmark in benchmarks:
        # performance evaluation
        HSPPerfs = {"cost": [], "time": []}
        SATPerfs = {"cost": [], "time": []}
        HSPFails = 0
        SATFails = 0
        benchFiles = os.listdir('./testFiles/' + benchmark)
        benchFiles.sort()
        for file in benchFiles:
            if (HSPFails < MAX_FAILS or SATFails < MAX_FAILS) and file != "domain.pddl":
                args = '-PArgs=-o,./testFiles/{0}/domain.pddl,-f,./testFiles/{0}/{1},-t,{2}'.format(benchmark, file,
                                                                                                    TIMEOUT)
                if HSPFails < MAX_FAILS:
                    print("{} {} with HSP".format(benchmark, file))
                    cost, time = evalMeanPerf(['./gradlew', '-q', 'runHSP', args])
                    if time is None:
                        HSPFails += 1
                    else:
                        HSPFails = 0
                    HSPPerfs["cost"].append(cost)
                    HSPPerfs["time"].append(time)
                else:
                    HSPPerfs["cost"].append(None)
                    HSPPerfs["time"].append(None)
                    print("\tSkipping, too many errors or timeout encountered in previous problems")

                print("{} {} with SAT".format(benchmark, file))
                if SATFails < MAX_FAILS:
                    cost, time = evalMeanPerf(['./gradlew', '-q', 'runSAT', args])
                    if time is None:
                        SATFails += 1
                    else:
                        SATFails = 0
                    SATPerfs["cost"].append(cost)
                    SATPerfs["time"].append(time)
                else:
                    SATPerfs["cost"].append(None)
                    SATPerfs["time"].append(None)
                    print("\tSkipping, too many errors or timeout encountered in previous problems")

        # plotting

        problems = range(1, len(HSPPerfs["cost"]) + 1)

        fig, ax = plt.subplots()

        plt.subplot(1, 2, 1)

        plt.plot(problems, HSPPerfs["cost"], 'o', label="A*")
        plt.plot(problems, SATPerfs["cost"], '.', label="SAT")
        plt.title("coût du plan")
        plt.legend()

        plt.subplot(1, 2, 2)

        plt.plot(problems, HSPPerfs["time"], 'o', label="A*")
        plt.plot(problems, SATPerfs["time"], '.', label="SAT")
        plt.title("temps d'exécution")
        plt.legend()
        plt.savefig("perfs_" + benchmark + ".png")

        fig.clear()
