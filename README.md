
Pour lancer le planner SAT, utiliser la commande suivante en remplaceant les
noms de fichiers par les bons:

./gradlew runSAT -PArgs=-o,domain.pddl,-f,problem.pddl

Pour lancer le script d'évaluation des performances, il faut lancer le script
evalPerfs.py avec pyhton 3. Vous devez avoir mathplotlib installé sur votre
ordinteur pour que le script marche.
