/**
 * The type Value.
 * Used to transmit information about a sat value
 */
public class Value {
  private boolean isOperator;
  private int index;
  private int state;

  /**
   * Create a new Value
   *
   * @param isOperator true if an operator, false if a fact
   * @param index      the index of the value in the problem structure
   * @param state      the state in which the value is defined
   */
  public Value(boolean isOperator, int index, int state) {
    this.isOperator = isOperator;
    this.index = index;
    this.state = state;
  }

  /**
   * Create a new Value
   *
   * @param isOperator true if an operator, false if a fact
   * @param index      the index of the value in the problem structure
   */
  public Value(boolean isOperator, int index) {
    this.isOperator = isOperator;
    this.index = index;
  }

  /**
   * Create a new empty Value
   */
  public Value() {
    index = -1;
    state = -1;
  }

  /**
   * Is operator boolean.
   *
   * @return the boolean
   */
  public boolean isOperator() {
    return isOperator;
  }

  /**
   * Sets operator.
   *
   * @param operator the operator
   */
  public void setOperator(boolean operator) {
    isOperator = operator;
  }

  /**
   * Gets index.
   *
   * @return the index
   */
  public int getIndex() {
    return index;
  }

  /**
   * Sets index.
   *
   * @param index the index
   */
  public void setIndex(int index) {
    this.index = index;
  }

  /**
   * Gets state.
   *
   * @return the state
   */
  public int getState() {
    return state;
  }

  /**
   * Sets state.
   *
   * @param state the state
   */
  public void setState(int state) {
    this.state = state;
  }
}
