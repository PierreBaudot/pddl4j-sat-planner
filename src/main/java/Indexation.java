import fr.uga.pddl4j.encoding.CodedProblem;

public class Indexation {
  private CodedProblem problem;
  private int sizeOperators;
  private int sizeFacts;

  /**
   * Create a new encoder
   *
   * @param problem the problem to be encode
   */
  public Indexation(CodedProblem problem) {
    this.problem = problem;
    sizeFacts = problem.getRelevantFacts().size();
    sizeOperators = problem.getOperators().size();
  }


  /**
   * Encode to be used by SAT planner
   *
   * @param ind index of the fact in the problem
   * @param state step of the SAT planner
   * @param isFact can be fact or operator
   */
  public int getIndex(int ind, int state,boolean isFact) {
    int index;

    if(isFact){
      index = ind+(state*(sizeFacts+sizeOperators)) + 1;
    }else{
      index = ind+(state*(sizeFacts+sizeOperators)) + 1 + sizeFacts;
    }

    return index;
  }

  /**
   * decode to be used by problem
   *
   * @param index index in the SAT planner
   */
  public Value getValue(int index) {
    Value val = new Value();
    val.setState((index-1) / (sizeOperators + sizeFacts));
    int ind = (index-1) % (sizeOperators + sizeFacts);
    if(ind >= sizeFacts){
      val.setOperator(true);
      val.setIndex(ind - sizeFacts);
    }
    else{
      val.setOperator(false);
      val.setIndex(ind);
    }
    return val;
  }

}

