import fr.uga.pddl4j.encoding.CodedProblem;
import fr.uga.pddl4j.util.BitOp;
import fr.uga.pddl4j.util.BitState;
import fr.uga.pddl4j.util.BitVector;
import org.sat4j.core.VecInt;
import org.sat4j.core.Vec;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

import java.util.Iterator;

/**
 * This class implements a planning problem/domain encoding into DIMACS
 *
 * @author H. Fiorino
 * @version 1.0 - 30.03.2021
 */
public final class SATEncoding {
    /*
     * A SAT problem in dimacs format is a list of int list a.k.a clauses
     */
    private IVec<IVecInt> dimacs;
    private CodedProblem problem;

    /*
     * Current number of steps of the SAT encoding
     */
    private int steps;
    private Indexation index;
    /**
     * Create a new SAT encoding and start to encode.
     *
     * @param problem the problem to encode in SAT
     * @param steps start to encode to step
     * @param index the encoder/decoder
     */
    public SATEncoding(final CodedProblem problem, final int steps, Indexation index) {
        super();
        this.problem = problem;
        dimacs = new Vec<>();
        this.steps = 0;
        this.index = index;
        // We get the initial state from the planning problem
        final BitState init = new BitState(problem.getInit());
        for (int i = 0; i < problem.getRelevantFacts().size(); i++){
            if (init.get(i)){
                VecInt vec = new VecInt();
                vec.push(index.getIndex(i,this.steps,true));
                dimacs.push(vec);
            }
            else {
                VecInt vec = new VecInt();
                vec.push(-index.getIndex(i,this.steps,true));
                dimacs.push(vec);
            }
        }
        //generate all clauses until steps
        while(this.steps < steps){
            IVec<IVecInt> res = generateClauses();
            Iterator<IVecInt> it = res.iterator();
            while(it.hasNext()){
                IVecInt vec = it.next();
                dimacs.push(vec);
            }
        }

    }

    /**
     * Sat encoding for next step
     *
     * @return all generate clauses
     */
    private IVec<IVecInt> generateClauses(){
        IVec<IVecInt> vec =  new Vec<>();
        VecInt vecInt;
        for (int i = 0; i < problem.getOperators().size(); i++) {
            final BitOp a = problem.getOperators().get(i);
            final BitVector precondPos = a.getPreconditions().getPositive();
            final BitVector precondNeg = a.getPreconditions().getNegative();
            final BitVector positive = a.getUnconditionalEffects().getPositive();
            final BitVector negative = a.getUnconditionalEffects().getNegative();
            for (int y = 0; y < problem.getRelevantFacts().size(); y++){
                if (precondPos.get(y)){
                    vecInt = new VecInt();
                    vecInt.push(index.getIndex(y,steps,true));
                    vecInt.push(-index.getIndex(i,steps,false));
                    vec.push(vecInt);
                    //System.out.println("precond :" + vec);
                }
                if (precondNeg.get(y)){
                    vecInt = new VecInt();
                    vecInt.push(-index.getIndex(y,steps,true));
                    vecInt.push(-index.getIndex(i,steps,false));
                    vec.push(vecInt);
                    //System.out.println("precondNeg :" + vec);
                }
                if (positive.get(y)){
                    vecInt = new VecInt();
                    vecInt.push(index.getIndex(y,steps + 1,true));
                    vecInt.push(-index.getIndex(i,steps,false));
                    vec.push(vecInt);
                    //System.out.println("positive 1 :" + vec);
                }
                if (negative.get(y)){
                    vecInt = new VecInt();
                    vecInt.push(-index.getIndex(y,steps + 1,true));
                    vecInt.push(-index.getIndex(i,steps,false));
                    vec.push(vecInt);
                    //System.out.println("negative 1 :" + vec);
                }
            }

            for (int z =i +1; z < problem.getOperators().size(); z++){
                vecInt = new VecInt();
                vecInt.push(-index.getIndex(z,steps ,false));
                vecInt.push(-index.getIndex(i,steps ,false));
                vec.push(vecInt);
            }
        }

        for (int y = 0; y < problem.getRelevantFacts().size(); y++){
            VecInt pos = new VecInt();
            VecInt neg = new VecInt();
            for (int i = 0; i < problem.getOperators().size(); i++) {
                if(problem.getOperators().get(i).getUnconditionalEffects().getPositive().get(y)){
                    pos.push(index.getIndex(i,steps,false));
                }
                if(problem.getOperators().get(i).getUnconditionalEffects().getNegative().get(y)){
                    neg.push(index.getIndex(i,steps,false));
                }
            }

            if(!pos.isEmpty()){
                pos.push(-index.getIndex(y,steps + 1,true));
                pos.push(index.getIndex(y,steps,true));
                vec.push(pos);
            }
            if(!neg.isEmpty()){
                neg.push(index.getIndex(y,steps + 1,true));
                neg.push(-index.getIndex(y,steps,true));
                vec.push(neg);
            }
        }
        steps+=1;
        return vec;
    }

    public IVec<IVecInt> getInit() {
        return dimacs;
    }

    /**
     * SAT encoding for next step
     * @return all generate clauses
     */
    public IVec<IVecInt> next() {
        return generateClauses();
    }

    /**
     * SAT encoding goals for next step
     *
     * @return all generate clauses
     */
    public IVec<IVecInt> nextGoal() {
        IVec<IVecInt> dimacsGoal = new Vec<>();
        final BitState goal = new BitState(problem.getGoal());
        for (int i = 0; i < problem.getRelevantFacts().size(); i++){
            if (goal.get(i)){
                VecInt vec = new VecInt();
                vec.push(index.getIndex(i,steps,true));
                dimacsGoal.push(vec);
            }
        }
        return dimacsGoal;
    }
}