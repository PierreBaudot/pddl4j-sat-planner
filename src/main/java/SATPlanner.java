
import fr.uga.pddl4j.encoding.CodedProblem;
import fr.uga.pddl4j.heuristics.relaxation.FastForward;
import fr.uga.pddl4j.planners.Planner;
import fr.uga.pddl4j.planners.statespace.AbstractStateSpacePlanner;
import fr.uga.pddl4j.util.BitState;
import fr.uga.pddl4j.util.Plan;
import fr.uga.pddl4j.util.SequentialPlan;

import java.util.*;
import java.util.stream.Stream;

import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.*;

/**
 * This class implements a simple SAT planner based on SAT4J.
 *
 * @author H. Fiorino
 * @author L. Bernard
 * @author P. Baudot
 * @author Q. Démarque
 * @author A. Nowak
 * @version 1.0 - 29.03.2021
 */
public final class SATPlanner extends AbstractStateSpacePlanner {

  /*
   * The arguments of the planner.
   */
  private Properties arguments;
	
  /**
   * Creates a new SAT planner with the default parameters.
   *
   * @param arguments the arguments of the planner.
   */
  public SATPlanner(final Properties arguments) {
    super();
    this.arguments = arguments;
  }

  /**
   * Solves the planning problem and returns the first solution found.
   *
   * @param problem the problem to be solved.
   * @return a solution search or null if it does not exist.
   */
  @Override
  public Plan search(final CodedProblem problem) {
    // The solution plan is sequential
    final Plan plan = new SequentialPlan();
    // We get the initial state from the planning problem
    final BitState init = new BitState(problem.getInit());
    // We get the goal from the planning problem
    final BitState goal = new BitState(problem.getGoal());
    // Nothing to do, goal is already satisfied by the initial state
    if (init.satisfy(problem.getGoal())) {
      return plan;
    }
    // Otherwise, we start the search
    else {
      Indexation ind = new Indexation(problem);
      // SAT solver timeout
      final int timeout = ((int) this.arguments.get(Planner.TIMEOUT));

      final FastForward ff = new FastForward(problem);
      final int minSteps = ff.estimate(init,problem.getGoal());

      System.out.println("MinSteps :" + minSteps);

      SATEncoding encoder = new SATEncoding(problem, minSteps, ind);
      ISolver solver = SolverFactory.newDefault();
      solver.setTimeout(timeout);

      // SAT solver max number of var
      final int MAXVAR = 1000000;
      // SAT solver max number of clauses
      final int NBCLAUSES = 500000;
      // Prepare the solver to accept MAXVAR variables. MANDATORY for MAXSAT solving
      solver.newVar(MAXVAR);
      solver.setExpectedNumberOfClauses(NBCLAUSES);

      // SAT Encoding starts here!
      //final int steps = (int) arguments.get("steps");
      IProblem ip ;

      try {
        solver.addAllClauses(encoder.getInit());
        ip = encode(solver, encoder);
        // We are done. Working now on the IProblem interface
        int[] model = ip.model();
        Arrays.stream(model).forEach(i -> {
          if(i > 0) {
            Value val = ind.getValue(i);
            if (val.isOperator()) {
              plan.add(val.getState(), problem.getOperators().get(val.getIndex()));
            }
          }
        });
      } catch (ContradictionException e){
        System.out.println("SAT encoding failure! :D");
        System.err.println(e);
        System.exit(0);
      } catch (TimeoutException e){
        System.out.println("Timeout! No solution found!");
        System.exit(0);
      } catch (EncodingException e){
        System.out.println("Error while encoding");
        System.exit(0);
      }catch (Exception e){
        System.out.println("Error");
        System.err.println(e);
        System.exit(0);
      }

      // Finally, we return the solution plan or null otherwise
      return plan;
    }
  }

  /**
   * Search and return a satisfiable instance of the sat problem.
   *
   * @param solver the problem to be solved.
   * @param encoder used to encode the problem in sat form.
   * @return a satisfiable instance of the problem.
   */
  private IProblem encode(ISolver solver, SATEncoding encoder) throws TimeoutException, ContradictionException, EncodingException {
    IVec<IVecInt> clauses = encoder.next();
    solver.addAllClauses(clauses);
    ArrayList<IConstr> constr = new ArrayList<>();
    IVec<IVecInt> goal = encoder.nextGoal();
    Iterator<IVecInt> it = goal.iterator();

    while(it.hasNext()){
      IVecInt vec = it.next();
      constr.add(solver.addClause(vec));
    }
    if (solver.isSatisfiable()) {
      return solver;
    } else {
      Stream<Boolean> res = constr.stream().map(solver::removeConstr);
      if(res.anyMatch(b -> !b)) throw new EncodingException();
      return encode(solver, encoder);
    }
  }
}